const request = require('supertest');
const app = require('../src/app.js');
const User = require('../src/models/user')

const userOne = {
    name: "karthi",
    email: "karthick123@gmail.com",
    password: "Karthi1496"
}

beforeEach(async () => {
    await User.deleteMany()
    await new User(userOne).save()
})

test('should signup', async () => {
    await request(app).post('/users').send({
        name: "karthick",
        email: "karthick11@gmail.com",
        password: "Karthi1496"
    }).expect(201)
})
test('should login', async () => {
    await request(app).post('/users/login').send({

        email: userOne.email,
        password: userOne.password
    }).expect(200)
})