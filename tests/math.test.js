const { ftoC, ctoF } = require('../src/math.js');

test('ftoc', () => {
    const fahrenheit = ftoC(32)
    expect(fahrenheit).toBe(0)
})
test('ftoc', () => {
    const celcius = ctoF(0)
    expect(celcius).toBe(32)
})