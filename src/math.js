const ftoC = (temp) => {
    return (temp - 32) / 1.8
}
const ctoF = (temp) => {
    return (temp * 1.8) + 32
}

module.exports = {
    ftoC,
    ctoF
}