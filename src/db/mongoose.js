const mongoose = require('mongoose')

// const MONGO_URL = process.env.MONGO_URL

mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useCreateIndex: true

})
// const db = mongoose.connection
// db.on('error', (error) => {
//     console.log('not connected')
// })
// db.once('open', () => {
//     console.log('success')
// })